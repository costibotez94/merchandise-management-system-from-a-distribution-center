package Anexe;

import Tabele.StructuraTabelActReceptie;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;

public class ActDeReceptie extends JFrame
{
    JTable tabelPrimire;
    JPanel pnlTabel;
    JButton btnAct;

    String numeFurnizor;
    int totalMarfuri;
    float cantitate, pretUnitar, pretFTVA, TVA, pretTotal;
    
    public ActDeReceptie(String numeFurnizor, int totalMarfuri, float cantitate,
                         float pretUnitar, float pretFTVA, float TVA, float pretTotal)
    {
        super("Act de receptie");
        this.numeFurnizor = numeFurnizor;
        this.totalMarfuri = totalMarfuri;
        this.cantitate = cantitate;
        this.pretUnitar = pretUnitar;
        this.pretFTVA = pretFTVA;
        this.TVA = TVA;
        this.pretTotal = pretTotal;
        
        
        btnAct = new JButton("Inregistreaza");
        tabelPrimire = new JTable(new StructuraTabelActReceptie());
        pnlTabel = new JPanel();
        
        AscultatorActReceptie aar = new AscultatorActReceptie();
        btnAct.addActionListener(aar);
        
        tabelPrimire.setValueAt(numeFurnizor, 0, 0);
        tabelPrimire.setValueAt(totalMarfuri, 0, 1);
        tabelPrimire.setValueAt(cantitate, 0, 2);
        tabelPrimire.setValueAt(pretUnitar, 0, 3);
        tabelPrimire.setValueAt(pretFTVA, 0, 4);
        tabelPrimire.setValueAt(TVA, 0, 5);
        tabelPrimire.setValueAt(pretTotal, 0, 6);
        
        pnlTabel.add(tabelPrimire.getTableHeader());
        pnlTabel.add(tabelPrimire);
        pnlTabel.add(btnAct);
        
        getContentPane().add(pnlTabel, BorderLayout.CENTER);
    }

    public class AscultatorActReceptie implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource() == btnAct)
            {
                try
                {
                    PrintWriter pw = new PrintWriter(new FileWriter("receptie.txt", true));
                    pw.write("Furnizor: " + tabelPrimire.getValueAt(0, 0) + "\r\n" +
                             "Total marfuri: " + tabelPrimire.getValueAt(0, 1) + "\r\n" +
                             "Total greutate: " + tabelPrimire.getValueAt(0, 2) + "\r\n" +
                             "Pret/kg: " + tabelPrimire.getValueAt(0, 3) + "\r\n" +
                             "Pret fara TVA: " + tabelPrimire.getValueAt(0, 4) + "\r\n" +
                             "TVA: " + tabelPrimire.getValueAt(0, 5) + "\r\n" +
                             "Pret total: " + tabelPrimire.getValueAt(0, 6) + "\r\n" + "\r\n");
                    
                    pw.flush();
                    pw.close();
                    JOptionPane.showMessageDialog(null, "S-a inregistrat cu succes!");
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }
            }
                
        }
    }
}
