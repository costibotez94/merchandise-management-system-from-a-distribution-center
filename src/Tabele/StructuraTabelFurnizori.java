package Tabele;

import Furnizori.ListaFurnizori;
import javax.swing.table.AbstractTableModel;

public class StructuraTabelFurnizori extends AbstractTableModel
{
    String[] numeColoane =  { 
                                "Furnizor",
                                "Adresa",
                                "Cont bancar"
                            };
    Object[][] date;
    
    public StructuraTabelFurnizori(ListaFurnizori listaFurnizori)
    {
        date = new Object[listaFurnizori.size()][getColumnCount()];
        for(int i=0; i<listaFurnizori.size(); i++)
        {
            date[i][0] = listaFurnizori.getFurnizor(i).getNume();
            date[i][1] = listaFurnizori.getFurnizor(i).getAdresa();
            date[i][2] = listaFurnizori.getFurnizor(i).getContBancar();
        }
    }
    
    @Override
    public int getRowCount() 
    {
        return date.length;
    }

    @Override
    public int getColumnCount() 
    {
        return numeColoane.length;
    }

    @Override
    public Object getValueAt(int linie, int coloana) {
        return date[linie][coloana];
    }
    
    @Override public String getColumnName(int coloana) 
    { 
        return numeColoane[coloana]; 
    }
    
    @Override
    public void setValueAt(Object value, int linie, int coloana)
    {
        date[linie][coloana] = value;
        fireTableCellUpdated(linie, coloana);
    }
}
