package Tabele;

import Autovehicule.ListaAutovehicule;
import javax.swing.table.AbstractTableModel;

public class StructuraTabelAutovehicule extends AbstractTableModel
{
    String[] numeColoane =  { 
                                "Denumire",
                                "Capacitate",
                                "Stoc"
                            };
    Object[][] date;
    
    public StructuraTabelAutovehicule(ListaAutovehicule listaAutovehicule)
    {
        date = new Object[listaAutovehicule.size()][getColumnCount()];
        for(int i=0; i<listaAutovehicule.size(); i++)
        {
            date[i][0] = listaAutovehicule.getAutovehicul(i).getDenumire();
            date[i][1] = listaAutovehicule.getAutovehicul(i).getCapacitate();
            date[i][2] = listaAutovehicule.getAutovehicul(i).getStoc();
        }
    }
    
    @Override
    public int getRowCount() 
    {
        return date.length;
    }

    @Override
    public int getColumnCount() 
    {
        return numeColoane.length;
    }

    @Override
    public Object getValueAt(int linie, int coloana) {
        return date[linie][coloana];
    }
    
    @Override public String getColumnName(int coloana) 
    { 
        return numeColoane[coloana]; 
    }
    
    @Override
    public void setValueAt(Object value, int linie, int coloana)
    {
        date[linie][coloana] = value;
        fireTableCellUpdated(linie, coloana);
    }
}
