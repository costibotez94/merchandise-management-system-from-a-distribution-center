package Tabele;

import FoiDeParcurs.FoaieDeParcurs;
import FoiDeParcurs.ListaFoiDeParcurs;
import Marfuri.Marfa;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class TabelExpediereMarfa extends JFrame
{
    JTable tabelExpediereMarfa;
    JPanel pnlExpediere, pnlButon;
    JButton btnExpediere;
    
    public TabelExpediereMarfa(ArrayList<Marfa> marfuri, String tipMasina, String numeSofer)
    {
        super("Tabel expediere marfa");
        
        tabelExpediereMarfa = new JTable(new StructuraTabelExpediereMarfa(marfuri, tipMasina, numeSofer));
        tabelExpediereMarfa.getColumnModel().getColumn(5).setPreferredWidth(100);
        tabelExpediereMarfa.getColumnModel().getColumn(6).setPreferredWidth(100);
        
        pnlExpediere = new JPanel(new GridLayout(1, 1));
        pnlButon = new JPanel(new FlowLayout());
        btnExpediere = new JButton("Expediaza");
        btnExpediere.addActionListener(new AscultatorTabelExpediere());
        
        JScrollPane scrollPane = new JScrollPane(tabelExpediereMarfa);
        scrollPane.setPreferredSize(new Dimension(getWidth(), 20));
        
        pnlExpediere.add(scrollPane);
        pnlButon.add(btnExpediere);
        
        getContentPane().add(pnlExpediere);
        getContentPane().add(pnlButon, BorderLayout.SOUTH);
    }
    
    private class AscultatorTabelExpediere implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e) 
        {
            if(e.getSource() == btnExpediere)
            {
                scrieFoiDeParcurs();               
                JOptionPane.showMessageDialog(null, "Foaie de parcurs creata pentru " + tabelExpediereMarfa.getValueAt(0, 6));
                dispose();
            }
        }

        private void scrieFoiDeParcurs() 
        {
            ListaFoiDeParcurs listaFoi = new ListaFoiDeParcurs();
            
            for(int i=0; i<tabelExpediereMarfa.getRowCount(); i++)
                listaFoi.adaugaFoaie(new FoaieDeParcurs(tabelExpediereMarfa.getValueAt(i, 6).toString(),
                                                        Float.parseFloat(tabelExpediereMarfa.getValueAt(i, 1).toString()),
                                                        tabelExpediereMarfa.getValueAt(i, 2).toString(),
                                                        tabelExpediereMarfa.getValueAt(i, 3).toString(),
                                                        tabelExpediereMarfa.getValueAt(i, 4).toString(),
                                                        tabelExpediereMarfa.getValueAt(i, 5).toString()));
            listaFoi.salveazaFoaie();

        }
    }
}
