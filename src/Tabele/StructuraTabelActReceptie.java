package Tabele;

import javax.swing.table.AbstractTableModel;

public class StructuraTabelActReceptie extends AbstractTableModel 
{
    String[] numeColoane =  { 
                                "Furnizor",
                                "Total marfuri",
                                "Greutate (Kg)",
                                "Pret/Kg",
                                "Pret fara TVA",
                                "TVA",
                                "Pret total (RON)"
                            };
    Object[][] date = { 
                        {"Comanda", "", "", "", "", "", ""}
                      };
    
    @Override
    public int getRowCount() 
    {
        return date.length;
    }

    @Override
    public int getColumnCount() 
    {
        return numeColoane.length;
    }

    @Override
    public Object getValueAt(int linie, int coloana) {
        return date[linie][coloana];
    }
    
    @Override public String getColumnName(int coloana) 
    { 
        return numeColoane[coloana]; 
    }
    
    @Override
    public void setValueAt(Object value, int linie, int coloana)
    {
        date[linie][coloana] = value;
        fireTableCellUpdated(linie, coloana);
    }
}
