package Tabele;

import Autovehicule.ListaAutovehicule;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;

public class TabelAutovehicule extends JFrame
{
    JTable tabelAutovehicule;
    JPanel pnlAutovehicule;
    JButton btnAutovehicule;
    
    public TabelAutovehicule()
    {
        super("Autovehicule");

        btnAutovehicule = new JButton("OK");
        tabelAutovehicule = new JTable(new StructuraTabelAutovehicule(new ListaAutovehicule()));
        tabelAutovehicule.getColumnModel().getColumn(0).setPreferredWidth(90);
        pnlAutovehicule = new JPanel();
        
        btnAutovehicule.addActionListener(new AscultatorTabelAutovehicule());
        
        pnlAutovehicule.add(tabelAutovehicule.getTableHeader());
        pnlAutovehicule.add(tabelAutovehicule);
        pnlAutovehicule.add(btnAutovehicule);
        
        getContentPane().add(pnlAutovehicule, BorderLayout.CENTER);
    }
    
    private class AscultatorTabelAutovehicule implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            if(e.getSource() == btnAutovehicule)
            {
                dispose();
            }
        }
        
    }
}
