package Tabele;

import Marfuri.Marfa;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public final class StructuraTabelExpediereMarfa extends AbstractTableModel
{
    String[] numeColoane =  { 
                                "Marfa",
                                "Cantitate",
                                "U.M.",
                                "Destinatie",
                                "Prioritate",
                                "Tip Masina",
                                "Sofer"
                            };
    Object[][] date;
    
    public StructuraTabelExpediereMarfa(ArrayList<Marfa> marfuri, String tipMasina, String numeSofer)
    {
        date = new Object[marfuri.size()][getColumnCount()];
        
        for(int i=0; i<marfuri.size(); i++)
        {
            date[i][0] = "Marfa " + (i+1);
            date[i][1] = marfuri.get(i).getCantitate();
            date[i][2] = marfuri.get(i).getUM();
            date[i][3] = marfuri.get(i).getDestinatie();
            date[i][4] = marfuri.get(i).getPrioritate();
            date[i][5] = tipMasina;
            date[i][6] = numeSofer;
        }
    }
    
    @Override
    public int getRowCount() 
    {
        return date.length;
    }

    @Override
    public int getColumnCount() 
    {
        return numeColoane.length;
    }

    @Override
    public Object getValueAt(int linie, int coloana) {
        return date[linie][coloana];
    }
    
    @Override public String getColumnName(int coloana) 
    { 
        return numeColoane[coloana]; 
    }
    
    @Override
    public void setValueAt(Object value, int linie, int coloana)
    {
        date[linie][coloana] = value;
        fireTableCellUpdated(linie, coloana);
    }
}
