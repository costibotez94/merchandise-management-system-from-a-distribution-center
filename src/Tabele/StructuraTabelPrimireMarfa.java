package Tabele;

import Marfuri.Marfa;
import javax.swing.table.AbstractTableModel;
import static Meniuri.PrimireMarfa.isNumeric;
import javax.swing.JOptionPane;

public final class StructuraTabelPrimireMarfa extends AbstractTableModel
{
    String[] numeColoane =  { 
                                Marfa.class.getSimpleName(),
                                "Cantitate",
                                "U.M.",
                                "Destinatie",
                                "Prioritate",
                            };
    Object[][] date;
    
    public StructuraTabelPrimireMarfa(int rand)
    {
        date = new Object[rand][getColumnCount()];
        for(int i=0; i<rand; i++)
            date[i][0] = "Comanda" + (i+1);
        
    }
    
    @Override
    public int getRowCount() 
    {
        return date.length;
    }

    @Override
    public int getColumnCount() 
    {
        return numeColoane.length;
    }

    @Override
    public Object getValueAt(int linie, int coloana) {
        return date[linie][coloana];
    }
    
    @Override 
    public boolean isCellEditable(int linie, int coloana) 
    { 
        if (coloana == 0) 
            return false; 
        else
            return true;
    }
    
    @Override public String getColumnName(int coloana) 
    { 
        return numeColoane[coloana]; 
    }
    
    @Override
    public void setValueAt(Object value, int linie, int coloana) 
    {
        if(coloana != 1 && !isNumeric(value.toString()))
        {    
            date[linie][coloana] = value;
            fireTableCellUpdated(linie, coloana);
        }
        else
            if(coloana == 1 && isNumeric(value.toString()))
            {    
                date[linie][coloana] = value;
                fireTableCellUpdated(linie, coloana);
            }
            else
                 JOptionPane.showMessageDialog(null, "Introduceti o valoare intreaga");
    }
    
}