package Tabele;

import Furnizori.ListaFurnizori;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class TabelFurnizori extends JFrame
{
    JTable tabelFurnizori;
    JPanel pnlFurnizori, pnlButon;
    JButton btnFurnizori;
    
    public TabelFurnizori()
    {
        super("Furnizori");

        btnFurnizori = new JButton("OK");
        tabelFurnizori = new JTable(new StructuraTabelFurnizori(new ListaFurnizori()));
        pnlFurnizori = new JPanel(new GridLayout(1, 1));
        pnlButon = new JPanel(new FlowLayout());
        
        btnFurnizori.addActionListener(new AscultatorTabelFurnizori());

        JScrollPane scrollPane = new JScrollPane(tabelFurnizori);
        scrollPane.setPreferredSize(new Dimension(getWidth(), 20));
        
        pnlFurnizori.add(scrollPane);
        pnlButon.add(btnFurnizori);
        
        getContentPane().add(pnlFurnizori);
        getContentPane().add(pnlButon, BorderLayout.SOUTH);
    }
    
    private class AscultatorTabelFurnizori implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            if(e.getSource() == btnFurnizori)
            {
                dispose();
            }
        }
     
    }
}
