package Tabele;

import Angajati.ListaAngajati;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;

public class TabelAngajati extends JFrame
{
    JTable tabelAngajati;
    JPanel pnlAngajati;
    JButton btnAngajati;
    
    public TabelAngajati()
    {
        super("Angajati");

        btnAngajati = new JButton("OK");
        tabelAngajati = new JTable(new StructuraTabelAngajati(new ListaAngajati()));
        tabelAngajati.getColumnModel().getColumn(2).setPreferredWidth(100);
        pnlAngajati = new JPanel();

        btnAngajati.addActionListener(new AscultatorTabelAngajati());
        
        pnlAngajati.add(tabelAngajati.getTableHeader());
        pnlAngajati.add(tabelAngajati);
        pnlAngajati.add(btnAngajati);
        
        getContentPane().add(pnlAngajati, BorderLayout.CENTER);
    }
    
    private class AscultatorTabelAngajati implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e) 
        {
            if(e.getSource() == btnAngajati)
                dispose();
        }
        
    }
}
