package Tabele;

import Angajati.ListaAngajati;
import javax.swing.table.AbstractTableModel;

public final class StructuraTabelAngajati extends AbstractTableModel
{
    String[] numeColoane =  { 
                                "Nume",
                                "Prenume",
                                "CNP",
                                "Functie",
                                "Salariu",
                                "Vechime"
                            };
    Object[][] date;
    
    public StructuraTabelAngajati(ListaAngajati listaAngajati)
    {
        date = new Object[listaAngajati.size()][getColumnCount()];
        for(int i=0; i<listaAngajati.size(); i++)
        {
            date[i][0] = listaAngajati.getAngajat(i).getNume();
            date[i][1] = listaAngajati.getAngajat(i).getPrenume();
            date[i][2] = listaAngajati.getAngajat(i).getCNP();
            date[i][3] = listaAngajati.getAngajat(i).getFunctie();
            date[i][4] = listaAngajati.getAngajat(i).getSalariu();
            date[i][5] = listaAngajati.getAngajat(i).getVechime();
        }
    }
    
    @Override
    public int getRowCount() 
    {
        return date.length;
    }

    @Override
    public int getColumnCount() 
    {
        return numeColoane.length;
    }

    @Override
    public Object getValueAt(int linie, int coloana) {
        return date[linie][coloana];
    }
    
    @Override public String getColumnName(int coloana) 
    { 
        return numeColoane[coloana]; 
    }
    
    @Override
    public void setValueAt(Object value, int linie, int coloana)
    {
        date[linie][coloana] = value;
        fireTableCellUpdated(linie, coloana);
    }
}
