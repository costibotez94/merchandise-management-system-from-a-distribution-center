package Meniuri;

import Marfuri.ListaMarfuri;
import Marfuri.Marfa;
import Furnizori.ListaFurnizori;
import Furnizori.Furnizor;
import Anexe.ActDeReceptie;
import Anexe.Enumeratii;
import Tabele.StructuraTabelPrimireMarfa;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.*;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

public class PrimireMarfa extends JFrame
{
    JTextField txtNume, txtAdresa, txtContBancar, txtNumarPachete;
    JLabel lblNume, lblAdresa, lblContBancar, lblNumarPachete, lblSpatiu, lblTitlu;
    JPanel pnlDate, pnlTabel, pnlButoane, pnlTitlu;
    JButton btnAct, btnDepozit, btnOK;
    JTable tabelMarfa;
    JComboBox prioritati, destinatii, um;
    JScrollPane tabelScrol;
    TableColumn coloanaPrioritati, coloanaDestinatii, coloanaUm;
    ListaFurnizori listaFurnizori;
    ListaMarfuri listaMarfuri;
    Enumeratii enumeratii;
    AscultatorPrimireMarfa apm;
    Point p;

    public PrimireMarfa()
    {
        super("Primire marfa");
        listaFurnizori = new ListaFurnizori();
        listaMarfuri = new ListaMarfuri();
        enumeratii = new Enumeratii();
        
        lblNume = new JLabel("  Nume");
        lblAdresa = new JLabel("  Adresa");
        lblContBancar = new JLabel("  Cont bancar");
        lblNumarPachete = new JLabel("  Numar pachete");
        lblSpatiu = new JLabel("");
        lblTitlu = new JLabel("Datele furnizorului", JLabel.CENTER);
        
        txtNume = new JTextField(10);
        txtAdresa = new JTextField(10);
        txtContBancar = new JTextField(10);
        txtNumarPachete = new JTextField(10);
        
        btnOK = new JButton("OK"); 
        btnDepozit = new JButton("Depoziteaza");
        btnAct = new JButton("Act receptie");
        
        pnlTitlu = new JPanel();
        pnlTitlu.setLayout(new GridLayout(1,1)); 
        pnlTitlu.add(lblTitlu, JLabel.CENTER);
        
        pnlDate = new JPanel();
        pnlDate.setLayout(new GridLayout(5,2,10,10)); 
        pnlDate.setBorder(BorderFactory.createLineBorder(Color.black));
        pnlDate.add(lblNume);
        pnlDate.add(txtNume);
        pnlDate.add(lblAdresa);
        pnlDate.add(txtAdresa);
        pnlDate.add(lblContBancar);
        pnlDate.add(txtContBancar);
        pnlDate.add(lblNumarPachete);
        pnlDate.add(txtNumarPachete);
        pnlDate.add(lblSpatiu);
        pnlDate.add(btnOK);
        
        btnOK.addActionListener(new AscultatorPrimireMarfa());
        
        getContentPane().add(pnlTitlu, BorderLayout.NORTH);
        getContentPane().add(pnlDate, BorderLayout.CENTER);
        
    }
    
    public class AscultatorPrimireMarfa implements ActionListener
    {
        private JFrame frame;
        
        @Override
        public void actionPerformed(ActionEvent ae)
        {
            if(ae.getSource() == btnOK)
            {

                if(!isNumeric(txtNume.getText()) && !isNumeric(txtAdresa.getText()) &&
                   isNumeric(txtContBancar.getText()) && isNumeric(txtNumarPachete.getText()) &&
                        !txtNume.getText().equals("") && !txtAdresa.getText().equals(""))
                {
                    listaFurnizori.adaugaFurnizor(new Furnizor(txtNume.getText(), 
                                                               txtAdresa.getText(), 
                                                               txtContBancar.getText(), 
                                                               Integer.parseInt(txtNumarPachete.getText())));
                    listaFurnizori.salveazaFurnizor();

                    setSize(400, 300);               
                    
                    tabelMarfa = new JTable(new StructuraTabelPrimireMarfa(Integer.parseInt(txtNumarPachete.getText())))
                    {
                        @Override
                        public String getToolTipText(MouseEvent e) 
                        {
                            String tip = null;
                            p = e.getPoint();
                            int coloana = columnAtPoint(p);
                            int rand = rowAtPoint(p);
                            int coloanaTabel = convertColumnIndexToModel(coloana);

                            if(coloanaTabel == 1) 
                            {
                                TableModel model = getModel();
                                String valoare = (String)model.getValueAt(rand, coloanaTabel);
                                if(valoare.equals(""))
                                    tip = "Introduceti o valoare intreaga";
                                else
                                    tip = "Cantitate " + valoare;
                            }
                            return tip;
                        }
                    };
                    
                    tabelMarfa.setModel(new StructuraTabelPrimireMarfa(Integer.parseInt(txtNumarPachete.getText())));

                    tabelMarfa.createDefaultColumnsFromModel();

                    um = new JComboBox(enumeratii.um);
                    coloanaUm = tabelMarfa.getColumnModel().getColumn(2); 
                    coloanaUm.setCellEditor(new DefaultCellEditor(um));

                    destinatii = new JComboBox(enumeratii.zone);
                    coloanaDestinatii = tabelMarfa.getColumnModel().getColumn(3); 
                    coloanaDestinatii.setCellEditor(new DefaultCellEditor(destinatii));

                    prioritati = new JComboBox(enumeratii.prioritati);
                    coloanaPrioritati = tabelMarfa.getColumnModel().getColumn(4); 
                    coloanaPrioritati.setCellEditor(new DefaultCellEditor(prioritati));

                    btnAct = new JButton("Act de receptie");
                    btnDepozit = new JButton("Depoziteaza");

                    btnAct.addActionListener(this);
                    btnDepozit.addActionListener(this);

                    pnlButoane = new JPanel();
                    pnlButoane.setLayout(new GridLayout(2,1,80,80));
                    pnlButoane.add(btnAct);
                    pnlButoane.add(btnDepozit);
                    getContentPane().add(pnlButoane, BorderLayout.EAST);
                    
                    JScrollPane scrollPane = new JScrollPane(tabelMarfa);
                    scrollPane.setPreferredSize(new Dimension(getWidth(), 100));

                    pnlTabel = new JPanel(new BorderLayout(0, 0));
                    pnlTabel.add(scrollPane);
                    
                    getContentPane().add(pnlTabel, BorderLayout.SOUTH);
                }
                else
                    JOptionPane.showMessageDialog(null, "Nu e completat corect.");
            }
            
            if(ae.getSource() == btnAct)
            {
                try
                {
                    frame = new ActDeReceptie(txtNume.getText(), 
                                              Integer.parseInt(txtNumarPachete.getText()),
                                              calculeazaCantitate(), 
                                              12, 
                                              calculeazaPretFTVA(), 
                                              calculeazaTVA(), 
                                              calculeazaPretTotal());
                    frame.setSize(550, 110);
                    frame.setVisible(true);
                    frame.setResizable(false);
                    frame.setLocationRelativeTo(null);
                }
                catch(Exception e)
                {
                    JOptionPane.showMessageDialog(null, "Nu ati completat tabelul");
                }
            }
            
            if(ae.getSource() == btnDepozit)
            {
                try
                {
                    for(int i=0; i<tabelMarfa.getRowCount(); i++)
                    {
                        listaMarfuri.adaugaMarfa(new Marfa(Float.parseFloat(tabelMarfa.getValueAt(i, 1).toString()),
                                tabelMarfa.getValueAt(i, 2).toString(),
                                tabelMarfa.getValueAt(i, 3).toString(),
                                tabelMarfa.getValueAt(i, 4).toString()));
                        listaMarfuri.salveazaMarfa();
                    }
                    JOptionPane.showMessageDialog(null, "S-a depozitat cu succes!");
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public static boolean isNumeric(String str)  
    {  
        try  
        {  
          int d = Integer.parseInt(str);  
        }  
        catch(NumberFormatException nfe)  
        {  
          return false;  
        }  
        return true;  
    }
    
    public float calculeazaTVA()
    {
        float TVA = calculeazaPretTotal() * 0.24f;
        return TVA;
    }
    
    public float calculeazaPretFTVA()
    {
        float pretFTVA = calculeazaPretTotal()- calculeazaTVA();
        return pretFTVA;
    }
    
    public float calculeazaCantitate()
    {
        float cantitateTotala = 0f;
        for(int i=0; i<tabelMarfa.getRowCount()-1; i++)
            if(tabelMarfa.getValueAt(i, 2).equals("Kg"))
                cantitateTotala += Integer.parseInt(tabelMarfa.getValueAt(i, 1).toString());
            else
                cantitateTotala += Integer.parseInt(tabelMarfa.getValueAt(i, 1).toString())*1000;
        return cantitateTotala;
    }
    
    public float calculeazaPretTotal()
    {
        float pretTotal = calculeazaCantitate() * 12;   
        return pretTotal;
    }
}
