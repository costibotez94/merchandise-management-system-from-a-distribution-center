package Meniuri;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class CentruDistributie extends JFrame
{
    private final JButton btnPrimire, btnExpediere, btnInchidere, btnStoc;
    private final JPanel pnlDreapta, pnlStanga, pnlCentru;
    
    public CentruDistributie()
    {
        super("Centru de Distributie");
        
        btnPrimire = new JButton("Primirea marfii");
        btnExpediere = new JButton("Expedierea marfii");
        btnStoc = new JButton("Stoc");
        btnInchidere = new JButton("Inchidere");
        
        btnPrimire.addActionListener(new AscultatorCentru());
        btnExpediere.addActionListener(new AscultatorCentru());
        btnStoc.addActionListener(new AscultatorCentru());
        btnInchidere.addActionListener(new AscultatorCentru());
        
        pnlCentru = new JPanel();
        pnlStanga = new JPanel();
        pnlDreapta = new JPanel();

        pnlStanga.setLayout(new GridLayout(1, 1));
        pnlDreapta.setLayout(new GridLayout(1, 1));
        
        pnlDreapta.setPreferredSize(new Dimension(50, getHeight()));
        pnlStanga.setPreferredSize(new Dimension(50, getHeight()));
        
        pnlCentru.setLayout(new GridLayout(6, 1, 10, 10));
        pnlCentru.add(new JLabel("Selectati una dintre optiuni:",JLabel.CENTER));
        pnlCentru.add(btnPrimire);
        pnlCentru.add(btnExpediere);
        pnlCentru.add(btnStoc);
        pnlCentru.add(btnInchidere);
        
        getContentPane().add(pnlStanga, BorderLayout.WEST);
        getContentPane().add(pnlCentru, BorderLayout.CENTER);
        getContentPane().add(pnlDreapta, BorderLayout.EAST);
    }

    private class AscultatorCentru implements ActionListener 
    {
        private JFrame frame;
        @Override
        public void actionPerformed(ActionEvent e)
        {
            if(e.getSource() == btnPrimire)
            {
                frame = new PrimireMarfa();
                frame.setSize(300, 200);
                frame.setVisible(true);
                frame.setResizable(false);
                frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                frame.setLocationRelativeTo(null);
            }
            
            if(e.getSource() == btnExpediere)
            {
                frame = new ExpediereMarfa();
                frame.setSize(300, 200);
                frame.setVisible(true);
                frame.setResizable(false);
                frame.setLocationRelativeTo(null);
            } 
            
            if(e.getSource() == btnStoc)
            {
                frame = new Stoc();
                frame.setSize(300, 250);
                frame.setVisible(true);
                frame.setResizable(false);
                frame.setLocationRelativeTo(null);
            }
            
            if(e.getSource() == btnInchidere)
            {
                System.exit(0);
            }
        }
    }
    
    public static void main(String[] args)
    {
        JFrame frame = new CentruDistributie();
        frame.setSize(350,250); 
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
