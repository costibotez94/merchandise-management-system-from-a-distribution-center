package Meniuri;

import Anexe.Enumeratii;
import Angajati.ListaAngajati;
import Autovehicule.ListaAutovehicule;
import Marfuri.ListaMarfuri;
import Marfuri.Marfa;
import Tabele.TabelExpediereMarfa;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.border.EmptyBorder;

public class ExpediereMarfa extends JFrame
{
    private final JLabel lblSelectare, lblTipMasina, lblSofer, lblSPATIU;
    private final JPanel pnlPrincipal;
    private final JComboBox cbDestinatii, cbTipMasina, cbSofer;
    private final JButton btnOK;
    private final Enumeratii enumeratii;
    private final String SOFER = "Sofer";
    private final ListaAngajati listaAngajati;
    
    public ExpediereMarfa()
    {
        super("Expediere marfa");
        listaAngajati = new ListaAngajati();
        //interfata
        
        enumeratii = new Enumeratii(); 
        lblSelectare = new JLabel("Selectati destinatia");
        lblTipMasina = new JLabel("Tip masina");
        lblSofer = new JLabel("Sofer");
        lblSPATIU = new JLabel("");
        btnOK = new JButton("Ok");
        cbTipMasina = new JComboBox(enumeratii.tipMasina);
        cbDestinatii = new JComboBox(enumeratii.zone);
        cbSofer = new JComboBox();
        
        pnlPrincipal = new JPanel(new GridLayout(0, 2, 10, 10));
        pnlPrincipal.add(lblSPATIU);
        pnlPrincipal.add(lblSPATIU);
        pnlPrincipal.add(lblSelectare);
        pnlPrincipal.add(cbDestinatii);
        pnlPrincipal.add(lblTipMasina);
        pnlPrincipal.add(cbTipMasina);
        pnlPrincipal.add(lblSofer);
        pnlPrincipal.add(cbSofer);
        pnlPrincipal.add(lblSPATIU);
        pnlPrincipal.add(btnOK);
        
        btnOK.addActionListener(new AscultatorExpediereMarfa(new ListaMarfuri(), 
                                                             new ListaAngajati(), 
                                                             new ListaAutovehicule()));
        
        //raport ==> marfuri, angajati, autovehicule disponibile
        for(int i=0; i<listaAngajati.size(); i++)
            if(listaAngajati.getAngajat(i).getFunctie().equals(SOFER))
                cbSofer.addItem(listaAngajati.getAngajat(i).getNume() + " " + listaAngajati.getAngajat(i).getPrenume());
        

        getContentPane().add(pnlPrincipal);
        pnlPrincipal.setBorder(new EmptyBorder(10, 0, 10, 0));
    }
    
    public class AscultatorExpediereMarfa implements ActionListener
    {
        ListaMarfuri listaMarfuri;
        ListaAngajati listaAngajati; 
        ListaAutovehicule listaAutovehicule;
        private ArrayList<Marfa> marfuri;
        
        private AscultatorExpediereMarfa(ListaMarfuri listaMarfuri, 
                                         ListaAngajati listaAngajati, 
                                         ListaAutovehicule listaAutovehicule) 
        {
            this.listaMarfuri = listaMarfuri;
            this.listaAngajati = listaAngajati;
            this.listaAutovehicule = listaAutovehicule;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            JFrame frame;
            if(e.getSource() == btnOK)
            {
                marfuri = new ArrayList<>();
                for(int i=0; i<listaMarfuri.size(); i++)
                    if(listaMarfuri.getMarfa(i).getDestinatie().equals(cbDestinatii.getSelectedItem().toString()))
                        marfuri.add(listaMarfuri.getMarfa(i));
                
                frame = new TabelExpediereMarfa(marfuri, 
                                                cbTipMasina.getSelectedItem().toString(), 
                                                cbSofer.getSelectedItem().toString());
                frame.setSize(600, 140);
                frame.setVisible(true);
                frame.setResizable(false);
                frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                frame.setLocationRelativeTo(null);
            }
        }
    }
    
    public boolean ajungeGreutate()
    {
        return false;
    }
}
