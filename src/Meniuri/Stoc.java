package Meniuri;

import Tabele.TabelAngajati;
import Tabele.TabelAutovehicule;
import Tabele.TabelFurnizori;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class Stoc extends JFrame
{
    JPanel pnlButoane, pnlStanga, pnlDreapta;
    JButton btnAngajati, btnAutovehicule, btnFurnizori;
    public Stoc()
    {
        super("Stoc");
        
        pnlStanga = new JPanel(new GridLayout(1, 1));
        pnlDreapta = new JPanel(new GridLayout(1, 1));
        
        btnAngajati = new JButton("Angajati");
        btnAutovehicule = new JButton("Autovehicule");
        btnFurnizori = new JButton("Furnizori");
        
        btnAngajati.addActionListener(new AscultatorStoc());
        btnAutovehicule.addActionListener(new AscultatorStoc());
        btnFurnizori.addActionListener(new AscultatorStoc());
        
        
        pnlButoane = new JPanel(new GridLayout(5, 1, 10, 10));
        pnlButoane.add(new JLabel("Stoc pentru: ", JLabel.CENTER));
        pnlButoane.add(btnAngajati);
        pnlButoane.add(btnAutovehicule);
        pnlButoane.add(btnFurnizori);

        pnlDreapta.setPreferredSize(new Dimension(50, getHeight()));
        pnlStanga.setPreferredSize(new Dimension(50, getHeight()));
        
        getContentPane().add(pnlStanga, BorderLayout.WEST);
        getContentPane().add(pnlButoane, BorderLayout.CENTER);
        getContentPane().add(pnlDreapta, BorderLayout.EAST);
    }

    private class AscultatorStoc implements ActionListener
    {
        private JFrame frame;
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            
            if(e.getSource() == btnAngajati)
            {
                frame = new TabelAngajati();
                frame.setSize(500, 140);
                frame.setVisible(true);
                frame.setResizable(false);
                frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                frame.setLocationRelativeTo(null);
            }
            
            if(e.getSource() == btnAutovehicule)
            {
                frame = new TabelAutovehicule();
                frame.setSize(270, 160);
                frame.setVisible(true);
                frame.setResizable(false);
                frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                frame.setLocationRelativeTo(null);
            }
            
            if(e.getSource() == btnFurnizori)
            {
                frame = new TabelFurnizori();
                frame.setSize(270, 160);
                frame.setVisible(true);
                frame.setResizable(false);
                frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                frame.setLocationRelativeTo(null);
            }
        }

    }
}
