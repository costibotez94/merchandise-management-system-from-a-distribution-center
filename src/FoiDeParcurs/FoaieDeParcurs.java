package FoiDeParcurs;

public class FoaieDeParcurs 
{
    private final String numeSofer, UM, destinatie, prioritate, tipMasina;
    private final float cantitate;
    
    public FoaieDeParcurs(String numeSofer, float cantitate, String UM, String destinatie, 
                          String prioritate, String tipMasina)
    {
        this.numeSofer = numeSofer;
        this.cantitate = cantitate;
        this.UM = UM;
        this.destinatie = destinatie;
        this.prioritate = prioritate;
        this.tipMasina = tipMasina;
    }
    
    public String getNumeSofer()
    {
        return numeSofer;
    }
    
    public String getUM()
    {
        return UM;
    }
    
    public float getCantitate()
    {
        return cantitate;
    }
    
    public String getDestinatie()
    {
        return destinatie;
    }
    
    public String getTipMasina()
    {
        return tipMasina;
    }
    
    public String getPrioritate()
    {
        return prioritate;
    }

}
