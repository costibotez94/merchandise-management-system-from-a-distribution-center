package FoiDeParcurs;

import IO.Scriere;
import java.util.ArrayList;

public class ListaFoiDeParcurs 
{
    private final ArrayList<FoaieDeParcurs> listaFoi;
    
    public ListaFoiDeParcurs()
    {
        listaFoi = new ArrayList<>();
    }
    
    public void adaugaFoaie(FoaieDeParcurs foaie)
    {
        listaFoi.add(foaie);
    }
    
    public FoaieDeParcurs getFoaie(int pozitie)
    {
        return listaFoi.get(pozitie);
    }
    
    public void salveazaFoaie()
    {
        new Scriere().scriereFoaieDeParcurs(listaFoi);
    }
    
    public int size()
    {
        return listaFoi.size();
    }
}
