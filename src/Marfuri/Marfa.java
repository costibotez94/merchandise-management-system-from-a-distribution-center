package Marfuri;

public class Marfa
{
    private final float cantitate;
    private final String destinatie, prioritate, UM;
    
    public Marfa(float cantitate, String UM, String destinatie, String prioritate)
    {
        this.cantitate = cantitate;
        this.UM = UM;
        this.destinatie = destinatie;
        this.prioritate = prioritate;
    }
    
    public String getDestinatie()
    {
        return destinatie;
    }
    
    public String getPrioritate()
    {
        return prioritate;
    }
    
    public float getCantitate()
    {
        return cantitate;
    }
    
    public String getUM()
    {
        return UM;
    }

}
