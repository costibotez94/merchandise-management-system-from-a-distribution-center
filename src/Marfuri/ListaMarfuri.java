package Marfuri;

import IO.Citire;
import IO.Scriere;
import java.util.ArrayList;

public class ListaMarfuri 
{
    private ArrayList<Marfa> listaMarfuri;
    
    public ListaMarfuri()
    {
        new Citire().citireMarfuri();
    }
    
    public void adaugaMarfa(Marfa marfa) 
    {
        listaMarfuri.add(marfa);
    }
    
    public Marfa getMarfa(int pozitie)
    {
        return listaMarfuri.get(pozitie);
    }
    
    public void stergeMarfa(Marfa marfa)
    {
        for(Marfa marfuri: listaMarfuri)
            if(marfuri.equals(marfa))
                listaMarfuri.remove(marfa);
    }
    
    public void stergeMarfa(int pozitie)
    {
        if(pozitie<listaMarfuri.size() && pozitie>=0)
            listaMarfuri.remove(pozitie);
    }
    
    public void salveazaMarfa()
    {
        new Scriere().scriereMarfuri(listaMarfuri.get(listaMarfuri.size()-1));
    }
    
    public int size()
    {
        return listaMarfuri.size();
    }
}
