package Angajati;

public class Angajat 
{
    private final String nume, prenume, CNP, functie;
    private final float salariu;
    private final int vechime;
    
    public Angajat(String nume, String prenume, String CNP, String functie, float salariu, int vechime)
    {
        this.nume = nume;
        this.prenume = prenume;
        this.CNP = CNP;
        this.functie = functie;
        this.salariu = salariu;
        this.vechime = vechime;
    }
    
    public String getNume()
    {
        return nume;
    }
    
    public String getPrenume()
    {
        return prenume;
    }
    
    public String getCNP()
    {
        return CNP;
    }
    
    public String getFunctie()
    {
        return functie;
    }
    
    public float getSalariu()
    {
        return salariu;
    }
    
    public int getVechime()
    {
        return vechime;
    }
}
