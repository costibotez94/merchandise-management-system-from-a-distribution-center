package Angajati;

import IO.Citire;
import IO.Scriere;
import java.util.ArrayList;

public class ListaAngajati 
{
    private final ArrayList<Angajat> listaAngajati;
    
    public ListaAngajati()
    {
        listaAngajati = new Citire().citireAngajati();
    }
    public void adaugaAngajat(Angajat angajat)
    {
        listaAngajati.add(angajat);
    }
    
    public Angajat getAngajat(int pozitie)
    {
        return listaAngajati.get(pozitie);
    }
    
    public Angajat cautaAngajat(String CNP)
    {
        for(Angajat angajat: listaAngajati)
            if(angajat.getCNP().equals(CNP))
               return angajat;
        return null;
    }
    
    public void stergeAngajat(String CNP)
    {
        for(Angajat angajat: listaAngajati)
            if(angajat.getCNP().equals(CNP))
                listaAngajati.remove(angajat);
    }
    
    public void stergeAngajat(int pozitie)
    {
        if(pozitie>=0 && pozitie<listaAngajati.size())
            listaAngajati.remove(pozitie);
    }
    
    public void salveazaAngajat()
    {
        new Scriere().scriereAngajat(listaAngajati);
    }
    
    public int size()
    {
        return listaAngajati.size();
    }
}
