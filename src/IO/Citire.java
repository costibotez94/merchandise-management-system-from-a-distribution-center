package IO;

import Angajati.Angajat;
import Autovehicule.Autovehicul;
import Furnizori.Furnizor;
import Marfuri.Marfa;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Citire implements ICitire
{

    @Override
    public ArrayList<Furnizor> citireFurnizori() 
    {
        ArrayList<Furnizor> listaFurnizori;
        try
        {
           listaFurnizori = new ArrayList<>();
           BufferedReader br = new BufferedReader(new FileReader("furnizori.txt"));
           String nume = br.readLine();
           String adresa = br.readLine();
           String contBancar = br.readLine();
           int numarPachete = Integer.parseInt(br.readLine());
           listaFurnizori.add(new Furnizor(nume, adresa, contBancar, numarPachete));
           
           while(br.readLine() != null)
           {
               nume = br.readLine();
               adresa = br.readLine();
               contBancar = br.readLine();
               numarPachete = Integer.parseInt(br.readLine());
               listaFurnizori.add(new Furnizor(nume, adresa, contBancar, numarPachete));
           }
           return listaFurnizori;
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ArrayList<Marfa> citireMarfuri() 
    {
        ArrayList<Marfa> listaMarfa;
        try
        {
           listaMarfa = new ArrayList<>();
           BufferedReader br = new BufferedReader(new FileReader("marfuri.txt"));
           float cantitate= Float.parseFloat(br.readLine());
           String UM = br.readLine();
           String destinatie = br.readLine();
           String prioritate = br.readLine();
           
           listaMarfa.add(new Marfa(cantitate, UM, destinatie, prioritate));
           
           while(br.readLine() != null)
           {
               cantitate= Float.parseFloat(br.readLine());
               UM = br.readLine();
               destinatie = br.readLine();
               prioritate = br.readLine();
               
               listaMarfa.add(new Marfa(cantitate, UM, destinatie, prioritate));
           }
           return listaMarfa;
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ArrayList<Autovehicul> citireAutovehicule() 
    {
        ArrayList<Autovehicul> listaAutovehicule;
        try
        {
           listaAutovehicule = new ArrayList<>();
           BufferedReader br = new BufferedReader(new FileReader("autovehicule.txt"));
           String denumire = br.readLine();
           int capacitate = Integer.parseInt(br.readLine());
           int pret = Integer.parseInt(br.readLine());
           
           listaAutovehicule.add(new Autovehicul(denumire, capacitate, pret));
           
           while(br.readLine() != null)
           {
               denumire = br.readLine();
               capacitate = Integer.parseInt(br.readLine());
               pret = Integer.parseInt(br.readLine());
               
               listaAutovehicule.add(new Autovehicul(denumire, capacitate, pret));
           }
           return listaAutovehicule;
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ArrayList<Angajat> citireAngajati() 
    {
        ArrayList<Angajat> listaAngajati;
        try
        {
           listaAngajati = new ArrayList<>();
           BufferedReader br = new BufferedReader(new FileReader("angajati.txt"));
           String nume = br.readLine();
           String prenume = br.readLine();
           String CNP = br.readLine();
           String functie = br.readLine();
           float salariu = Float.parseFloat(br.readLine());
           int vechime = Integer.parseInt(br.readLine());
           
           listaAngajati.add(new Angajat(nume, prenume, CNP, functie, salariu, vechime));
           
           while(br.readLine() != null)
           {
               nume = br.readLine();
               prenume = br.readLine();
               CNP = br.readLine();
               functie = br.readLine();
               salariu = Float.parseFloat(br.readLine());
               vechime = Integer.parseInt(br.readLine());
               
               listaAngajati.add(new Angajat(nume, prenume, CNP, functie, salariu, vechime));
           }
           return listaAngajati;
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
}
