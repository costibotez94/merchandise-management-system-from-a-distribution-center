package IO;

import Angajati.Angajat;
import Autovehicule.Autovehicul;
import FoiDeParcurs.FoaieDeParcurs;
import Furnizori.Furnizor;
import Marfuri.Marfa;
import java.util.ArrayList;

public interface IScriere
{
    public void scriereFurnizori(Furnizor furnizor);
    public void scriereMarfuri(Marfa marfa);
    public void scriereAutovehicule(ArrayList<Autovehicul> listaAutovehicule);
    public void scriereAngajat(ArrayList<Angajat> listaAngajati);
    public void scriereFoaieDeParcurs(ArrayList<FoaieDeParcurs> listaFoiDeParcurs);
}
