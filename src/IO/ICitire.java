package IO;

import Angajati.Angajat;
import Autovehicule.Autovehicul;
import Furnizori.Furnizor;
import Marfuri.Marfa;
import java.util.ArrayList;

public interface ICitire 
{
    public ArrayList<Furnizor> citireFurnizori();
    public ArrayList<Marfa> citireMarfuri();
    public ArrayList<Autovehicul> citireAutovehicule();
    public ArrayList<Angajat> citireAngajati();
}
