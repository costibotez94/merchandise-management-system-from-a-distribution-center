package IO;

import Angajati.Angajat;
import Autovehicule.Autovehicul;
import FoiDeParcurs.FoaieDeParcurs;
import Furnizori.Furnizor;
import Marfuri.Marfa;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Scriere implements IScriere
{

    @Override
    public void scriereFurnizori(Furnizor furnizor) 
    {
        try
        {
            PrintWriter pw = new PrintWriter(new FileWriter("furnizori.txt", true));
            
            pw.write("\r\n" + 
                     furnizor.getNume() + "\r\n" +
                     furnizor.getAdresa() + "\r\n" + 
                     furnizor.getContBancar() + "\r\n" +
                     furnizor.getNumarPachete() + "\r\n");
            
            pw.flush();
            pw.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void scriereMarfuri(Marfa marfa) 
    {
        try
        {
            PrintWriter pw = new PrintWriter(new FileWriter("marfuri.txt", true));
            
            pw.println( "\r\n" +
                        marfa.getCantitate()+ "\r\n" + 
                        marfa.getUM() + "\r\n" + 
                        marfa.getDestinatie() + "\r\n" +
                        marfa.getPrioritate());    
            pw.flush();
            pw.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void scriereAutovehicule(ArrayList<Autovehicul> listaAutovehicule) 
    {
        try
        {
            PrintWriter pw = new PrintWriter(new FileWriter("autovehicule.txt", true));
            for(Autovehicul autovehicul: listaAutovehicule)
                pw.write(autovehicul.getDenumire() + "\r\n" +
                         autovehicul.getCapacitate() + "\r\n" + 
                         autovehicul.getStoc() + "\r\n" + "\r\n");
            pw.flush();
            pw.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void scriereAngajat(ArrayList<Angajat> listaAngajati) 
    {
        try
        {
            PrintWriter pw = new PrintWriter(new FileWriter("angajati.txt", true));
            for(Angajat angajat: listaAngajati)
                pw.write(angajat.getNume() + "\r\n" +
                         angajat.getPrenume() + "\r\n" + 
                         angajat.getCNP() + "\r\n" + 
                         angajat.getFunctie() + "\r\n" + 
                         angajat.getSalariu() + "\r\n" + 
                         angajat.getVechime() + "\r\n" + "\r\n");
            pw.flush();
            pw.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void scriereFoaieDeParcurs(ArrayList<FoaieDeParcurs> listaFoiDeParcurs) 
    {
        try
        {
            PrintWriter pw = new PrintWriter(new FileWriter("foi_de_parcurs.txt", true));
            for(FoaieDeParcurs foaieDeParcurs: listaFoiDeParcurs)
                pw.write(foaieDeParcurs.getNumeSofer() + "\r\n" +
                         foaieDeParcurs.getCantitate() + "\r\n" + 
                         foaieDeParcurs.getUM() + "\r\n" + 
                         foaieDeParcurs.getDestinatie() + "\r\n" + 
                         foaieDeParcurs.getPrioritate() + "\r\n" + 
                         foaieDeParcurs.getTipMasina() + "\r\n" + "\r\n");
            pw.flush();
            pw.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
    
}
