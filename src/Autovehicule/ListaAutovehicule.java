package Autovehicule;

import IO.Citire;
import IO.Scriere;
import java.util.ArrayList;

public class ListaAutovehicule 
{
    private final ArrayList<Autovehicul> listaAutovehicule;
    
    public ListaAutovehicule()
    {
        Citire citire = new Citire();
        listaAutovehicule = citire.citireAutovehicule();
    }
    
    public void adaugaAutovehicul(Autovehicul autovehicul)
    {
        listaAutovehicule.add(autovehicul);
    }
    
    public Autovehicul getAutovehicul(int pozitie)
    {
        return listaAutovehicule.get(pozitie);
    }
    
    public Autovehicul cautaAutovehicul(String denumire)
    {
        for(Autovehicul autovehicul: listaAutovehicule)
            if(autovehicul.getDenumire().equals(denumire))
               return autovehicul;
        return null;
    }
    
    public void stergeAutovehicul(String denumire, float capacitate)
    {
        for(Autovehicul autovehicul: listaAutovehicule)
            if(autovehicul.getDenumire().equals(denumire) && 
               autovehicul.getCapacitate() == capacitate)
                listaAutovehicule.remove(autovehicul);
    }
    
    public void stergeAutovehicul(int pozitie)
    {
        if(pozitie>=0 && pozitie<listaAutovehicule.size())
            listaAutovehicule.remove(pozitie);
    }
    
    public void salveazaAutovehicul()
    {
        new Scriere().scriereAutovehicule(listaAutovehicule);
    }
    
    public int size()
    {
        return listaAutovehicule.size();
    }
}
