package Autovehicule;

public class Autovehicul 
{
    private final int capacitate;
    private final int stoc;
    private final String denumire;
    
    public Autovehicul(String denumire, int capacitate, int stoc)
    {
        this.denumire = denumire;
        this.capacitate = capacitate;
        this.stoc = stoc;
    }
    
    public int getCapacitate()
    {
        return capacitate;
    }
    
    public int getStoc()
    {
        return stoc;
    }
    
    public String getDenumire()
    {
        return denumire;
    }
}
