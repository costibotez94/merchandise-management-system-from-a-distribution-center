package Furnizori;

public class Furnizor
{
    private final String nume, adresa, contBancar;
    private final int numarPachete;
    
    public Furnizor(String nume, String adresa, String contBancar, int numarPachete)
    {
        this.nume = nume;
        this.adresa = adresa;
        this.contBancar = contBancar;  
        this.numarPachete = numarPachete;
    }
    
    public String getNume()
    {
        return nume;
    }
    
    public String getAdresa()
    {
        return adresa;
    }
    
    public String getContBancar()
    {
        return contBancar;
    }
    
    public int getNumarPachete()
    {
        return numarPachete;
    }
}
