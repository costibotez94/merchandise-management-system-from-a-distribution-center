package Furnizori;

import IO.Citire;
import IO.Scriere;
import java.util.ArrayList;

public class ListaFurnizori
{
    private final ArrayList<Furnizor> listaFurnizori;
    
    public ListaFurnizori()
    {
        Citire citire = new Citire();
        listaFurnizori = citire.citireFurnizori();
    }
    
    public void adaugaFurnizor(Furnizor furnizor)
    {
        listaFurnizori.add(furnizor);
    }
    
    public Furnizor getFurnizor(int pozitie)
    {
        return listaFurnizori.get(pozitie);
    }
    
    public void stergeFurnizor(Furnizor furnizorul)
    {
        for(Furnizor furnizor: listaFurnizori)
            if(furnizor.equals(furnizorul))
                listaFurnizori.remove(furnizorul);
    }
    
    public void stergeFurnizor(int pozitie)
    {
        if(pozitie<listaFurnizori.size() && pozitie>=0)
            listaFurnizori.remove(pozitie);
    }
    
    public void salveazaFurnizor()
    {
        new Scriere().scriereFurnizori(listaFurnizori.get(listaFurnizori.size()-1));
    }
    
    public int size()
    {
        return listaFurnizori.size();
    }
}
