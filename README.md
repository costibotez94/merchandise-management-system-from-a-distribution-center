# Merchandise management system from a distribution center #

The application manage the ins and outs of goods from the distribution center. Following a request telephone distribution center sends a truck to a supplier (client) to retrieve a certain amount of goods. The merchandise is made up of a lot of packages, each of them having a destination (common or not) and a priority delivery. On arrival of the truck at the distribution center can draw up an act of reception of cargo, packages being stored temporarily in a warehouse for delivery.

Recipients are divided (after certain criteria related to geographic position) in several areas "logical" (districts, sectors). It is possible that a same recipient to appear in several areas, with multiple addresses. When a delivery is planned will be selected in order of priority goods delivery and will be grouped optimally depending on the destination and gauge cars available. After finishing the group will draw up roadmaps for each driver separately which will contain information relating to the transported goods, quantity, destination etc. 

Returning from a "shift delivery", based on the roadmap and content filled car will draw the appropriate measures as follows:

* for goods delivered will end bills to be sent to customers to payment delivery; 
* papers for the goods that have been returned by the recipients (refused) or could not be delivered for objective reasons.

The application can generate the following documents:

* Acts of reception of the goods taken from the supplier
* Bills necessary for the payment of goods delivered to customers
* Roadmaps of drivers
* Papers for the goods that could not be delivered.

This app was developed in 2014 and it was written in Java.